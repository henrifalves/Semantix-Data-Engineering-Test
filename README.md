## Recomendações para o projeto prático

Para rodar o projeto, é necessário o jupyter, e os imports: pyspark.sql, pyspark.sql.types e o re para regex,
caso não tenha algum desses pacotes, baixe com pip ou conda caso prefira,

```
pip install pyspark

conda install pyspark
```


## 1. Qual o objetivo do comando cache em Spark?

### Resposta: 

O cache tem como objetivo manter o dataset disponível em memória, evitando assim, uma nova leitura de dados que retornaria exatamente 
a mesma coisa, usar cache é bem útil quando se necessita do mesmo rdd, como em um loop por exemplo, ou em uma estrutura condicional 
onde cada caminho levará ao mesmo rdd com a transformação filter carregando implicações diferentes, sem o cache será necessário 
carregar duas vezes o mesmo conjunto resiliente de dados, com o cache os dados já estariam em memória e não trafegando pela rede, 
bastaria apenas aplicar o filtro diferente.

## 2. O mesmo código implementado em Spark é normalmente mais rápido que a implementação equivalente em MapReduce. Por quê?

### Resposta: 

Duas coisas agravam a vantagem do Spark em um banchmark com o MapReduce, como dito na questão anterior o Spark pode manter os seus 
conjuntos de dados na memória, o MapReduce mantém os dados no disco com o HDFS, fora isso, mesmo não usando o cache() no spark você 
tem naturalmente a fluidez no trabalho do Spark, em uma aplicação cuja saída de uma operação é a entrada de outra o Spark não aloca 
isso em memória ele apenas repassa para a entrada.

Dados da Apache confirmam que muitas vezes o Spark consegue executar aplicações até 100 vezes mais rápido em memória e 10 vezes mais 
rápido em disco do que o Hadoop MapReduce.

## 3. Qual é a função do SparkContext?

### Resposta: 
É o client do ambiente da aplicação do próprio Spark, e é ele que comanda, em sua aplicação, os serviços do spark, é por ele também que 
você configura os serviços internos e estabelece, em alto nível, uma conexão com seu ambiente, se enquadra em um dos primeiros passos de 
uma aplicação que use spark.


## 4. Explique com suas palavras o que é Resilient, Distributed Datasetss (RDD)

### Resposta: 
RDD são dados distribuídos imutáveis, basicamente são coleções de objetos leitura e partição, eles toleram falhas já que são 
computados em diferentes nodes no cluster.
Os RDDs são pedras fundamentais para a abstração do Spark, sobre sua criação pode ser em um ambiente mais estável com disco ou 
uma cloud, mas também podem ser gerados por outros conjuntos de dados resilientes que recebem uma operação, por exemplo, um RDD 
pode ser qualquer tipo de objeto das linguagens que usam spark em suas aplicações inclusive classes criadas pelo próprio usuário, 
o conceito de RDD e sua implicação de funcionamento, também faz o Spark ser mais rápido que o MapReduce.



## 5. O GroupByKey é menos eficiente que reduceByKey em grandes dataset. Por quê?

### Resposta: 
ReduceByKey é mais eficiente pois ele agrega todos os valores do dataset baseados na função informada em seu parâmetro para um chave
comum criando uma saída menor, já o GroupByKey implica em retornar um par de chave e valor onde o valor é iterado em função da chave,
isso significa que ele retorna todas as chaves e valores, e depois os combina, a diferença prática na arquitetura entre eles é uma flag
que está desativada no GroupByKey.

## 6. Explique o que o código Scala abaixo faz.

```scala
val textFile = sc.textFile("hdfs://...")
val counts = textFile.flatMap(line => line.split(" ")) .map(word => (word, 1)) .reduceByKey(_ + _)
 counts.saveAsTextFile("hdfs://...")
```

### Resposta: 
A primeira linha é a criação de um conjunto de dados resilientes a partir de um path que leva a um Hadoop Distributed File System, 
esse valor é atribuído a um valor imutável denominado ai como textFile.

A segunda linha é uma transformação flatMap que índica ao spark que ele deve colocar em um único vetor todos os resultados gerados 
pela expressão lambda definida, essa lambda por sua vez significa que ela vai pegar o objeto da implicação e fatiar ele com base 
nos espaços, basicamente ele transforma um texto em um vetor de conjuntos de caracteres que não contém um espaço entre seus itens, 
palavras, basicamente, logo em seguida ele usa um map para fazer uma relação chave-valor onde a chave é um item da matriz iterada 
anteriormente e o valor é o número 1, logo em seguida as chaves iguais são agrupadas e seus valores somados, o resultado final é 
uma estrutura que contém a palavra (chave) e o número de vezes (valor) que ela foi encontrada no texto.
Essa estrutura é salva em um Hadoop Distributed File System pela função saveAsTextFile().
